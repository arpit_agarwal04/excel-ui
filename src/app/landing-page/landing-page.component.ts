import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.scss']
})
export class LandingPageComponent implements OnInit {

  rows: number = 20;

  column: number = 10;

  dataArray: any = [];

  dragStartIndex = { rowIndex: 0, columnIndex: 0 };

  dragEndIndex = { rowIndex: 0, columnIndex: 0 };

  sortDirection = '';

  sortColumnIndex = -1;

  blankSortedArray = [];

  // enableCellIndex = { rowIndex: null, columnIndex: null };

  constructor() { }

  ngOnInit(): void {
    for (let i = 0; i < this.rows; i++) {
      let arr = [];
      for (let j = 0; j < this.column; j++) {
        arr.push('');
      }
      this.dataArray.push(arr);
    }
  }

  createArray(length = 0): any[] {
    return Array(length);
  }

  printUpdatedValues(row: any, column: any) {
    if (Number(this.dataArray[row][column])) {
      this.dataArray[row][column] = parseFloat(this.dataArray[row][column]);
    }
    console.log(this.dataArray);
    this.blankSortedArray = this.deepCLone(this.dataArray);
    // this.enableCellIndex = { rowIndex: null, columnIndex: null };;
  }

  // enableEditing(row: any, column: any) {
  //   this.enableCellIndex.rowIndex = row;
  //   this.enableCellIndex.columnIndex = column;
  //   console.log('Edting:', row, column);
  // }

  deepCLone(arr: any) {
    return JSON.parse(JSON.stringify(arr));
  }

  onSort(key: number): void {
    if (this.sortColumnIndex === key) {
      if (this.sortDirection === 'asc') {
        this.sortDirection = 'desc';
        this.sortColumnIndex = key;
      } else if (this.sortDirection === 'desc') {
        this.sortDirection = '';
        this.sortColumnIndex = -1;
      }
    }
    else {
      this.sortDirection = 'asc'
      this.sortColumnIndex = key;
    }
    if (this.sortDirection === '') {
      this.dataArray = this.deepCLone(this.blankSortedArray);
    }
    else {
      let dataLength = this.rows - 1;
      let sortStartIndex = 0
      for (let i = 0; i < this.rows; i++) {
        if (this.dataArray[i].some((el: any) => el !== '')) {
          sortStartIndex = i;
          break;
        }
      }
      for (let i = this.rows - 1; i > 0; i--) {
        if (this.dataArray[i].some((el: any) => el !== '')) {
          dataLength = i;
          break;
        }
      }
      if (sortStartIndex !== dataLength) {
        this.mergeSort(this.dataArray, 0, dataLength, key, this.sortDirection);
      }
    }
    console.log('Sorted Array:', this.dataArray);
  }

  // merge sort method
  mergeSort(items: [], l: number, r: number, key: number, order: string): any {
    if (l >= r) {
      return;
    }
    const m = Math.floor((l + r - 1) / 2);
    this.mergeSort(items, l, m, key, order);
    this.mergeSort(items, m + 1, r, key, order);
    this.merge(items, l, m, r, key, order);
    return items;
  }

  // merge method
  merge(items: any, l: number, m: number, r: number, key: number, order: string): void {
    const n1 = m - l + 1;
    const n2 = r - m;
    const leftArray = new Array(n1);
    const rightArray = new Array(n2);
    for (let x = 0; x < n1; x++) {
      leftArray[x] = items[l + x];
    }
    for (let y = 0; y < n2; y++) {
      rightArray[y] = items[m + 1 + y];
    }
    let i = 0;
    let j = 0;
    let k = l;
    if (order === 'asc') {
      while (i < n1 && j < n2) {
        if (typeof leftArray[i][key] === 'number' && typeof rightArray[j][key] === 'number') {
          if (leftArray[i][key] <= rightArray[j][key]) {
            items[k] = leftArray[i];
            i++;
          }
          else {
            items[k] = rightArray[j];
            j++;
          }
        }
        else if (typeof leftArray[i][key] === 'number') {
          if (leftArray[i][key].toString().toLowerCase() <= rightArray[j][key].toLowerCase()) {
            leftArray[i][key] = parseFloat(leftArray[i][key]);
            items[k] = leftArray[i];
            i++;
          } else {
            items[k] = rightArray[j];
            j++;
          }
        }
        else if (typeof rightArray[j][key] === 'number') {
          if (leftArray[i][key].toLowerCase() <= rightArray[j][key].toString().toLowerCase()) {
            items[k] = leftArray[i];
            i++;
          } else {
            rightArray[j][key] = parseFloat(rightArray[j][key]);
            items[k] = rightArray[j];
            j++;
          }
        }
        else {
          if (leftArray[i][key].toLowerCase() <= rightArray[j][key].toLowerCase()) {
            items[k] = leftArray[i];
            i++;
          } else {
            items[k] = rightArray[j];
            j++;
          }
        }
        k++;
      }
      while (i < n1) {
        items[k] = leftArray[i];
        i++;
        k++;
      }
      while (j < n2) {
        items[k] = rightArray[j];
        j++;
        k++;
      }
    } else {
      while (i < n1 && j < n2) {
        if (typeof leftArray[i][key] === 'number' && typeof rightArray[j][key] === 'number') {
          if (leftArray[i][key] >= rightArray[j][key]) {
            items[k] = leftArray[i];
            i++;
          }
          else {
            items[k] = rightArray[j];
            j++;
          }
        }
        else if (typeof leftArray[i][key] === 'number') {
          if (leftArray[i][key].toString().toLowerCase() >= rightArray[j][key].toLowerCase()) {
            leftArray[i][key] = parseFloat(leftArray[i][key]);
            items[k] = leftArray[i];
            i++;
          } else {
            items[k] = rightArray[j];
            j++;
          }
        }
        else if (typeof rightArray[j][key] === 'number') {
          if (leftArray[i][key].toLowerCase() >= rightArray[j][key].toString().toLowerCase()) {
            items[k] = leftArray[i];
            i++;
          } else {
            rightArray[j][key] = parseFloat(rightArray[j][key]);
            items[k] = rightArray[j];
            j++;
          }
        }
        else {
          if (leftArray[i][key].toLowerCase() >= rightArray[j][key].toLowerCase()) {
            items[k] = leftArray[i];
            i++;
          } else {
            items[k] = rightArray[j];
            j++;
          }
        }
        k++;
      }
      while (i < n1) {
        items[k] = leftArray[i];
        i++;
        k++;
      }
      while (j < n2) {
        items[k] = rightArray[j];
        j++;
        k++;
      }
    }
  }

  copyFrom(row: any, column: any) {
    // console.log('CopyFrom:', row, column);
    this.dragStartIndex.rowIndex = row;
    this.dragStartIndex.columnIndex = column;
  }

  copyTo(row: any, column: any) {
    this.dragEndIndex.rowIndex = row;
    this.dragEndIndex.columnIndex = column;
    this.copyData();
    // console.log('CopyTo:', row, column);
  }

  copyData() {
    let copyValue = this.dataArray[this.dragStartIndex.rowIndex][this.dragStartIndex.columnIndex];
    // For same cell
    if (this.dragStartIndex.rowIndex === this.dragEndIndex.rowIndex && this.dragStartIndex.columnIndex === this.dragEndIndex.columnIndex) {
      // console.log('same');
    }
    // Drag and copy data horizontally
    else if (this.dragStartIndex.rowIndex === this.dragEndIndex.rowIndex) {
      for (let i = this.dragStartIndex.columnIndex + 1; i <= this.dragEndIndex.columnIndex; i++) {
        this.dataArray[this.dragStartIndex.rowIndex][i] = copyValue;
      }
    }
    // Drag and copy data vertically
    else if (this.dragStartIndex.columnIndex === this.dragEndIndex.columnIndex) {
      for (let i = this.dragStartIndex.rowIndex + 1; i <= this.dragEndIndex.rowIndex; i++) {
        this.dataArray[i][this.dragStartIndex.columnIndex] = copyValue;
      }
    }
    //drag and copy both horizontally and vertically
    else {
      for (let i = this.dragStartIndex.columnIndex + 1; i <= this.dragEndIndex.columnIndex; i++) {
        for (let j = this.dragStartIndex.rowIndex + 1; j <= this.dragEndIndex.rowIndex; j++) {
          this.dataArray[i][j] = copyValue;
        }
      }
    }
    console.log(this.dataArray);
  }

}
